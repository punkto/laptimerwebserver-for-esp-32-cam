/*
   Based on the AdvancedWebServer WebServer library example for Arduino by
   Majenko Technologies.
   
   
   Using a ESP32 Cam
   Pins: 
    - IO2 -> IR Sensor
*/

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>


const int ANTIREB_MILLIS = 500;
CHANGE  const char *ssid = "";
THIS    const char *password = "";
const int led = 4;
const int button = 2;
const int TIMES_ARRAY_LIMIT = 20;
const int TIMES_ARRAY_MAX_LENGTH = 14;
const int TEMP_STRING_LENGTH = 400 + TIMES_ARRAY_MAX_LENGTH * TIMES_ARRAY_LIMIT;

char temp[TEMP_STRING_LENGTH];
char laps_str[TIMES_ARRAY_MAX_LENGTH * TIMES_ARRAY_LIMIT];

struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
  int lastPressedMillis;
};
Button button1 = {button, 0, false, 0};

struct LastLapTime {
  int millisecs;
  int secs;
};
struct LastLapTime lastLapTimes[TIMES_ARRAY_LIMIT];
int lastLapTimesIndex = 0;

void init_lastLapTimes(){
  for (int i = 0; i < TIMES_ARRAY_LIMIT; i++){
    lastLapTimes[i].millisecs = 0;
    lastLapTimes[i].secs = 0;
  }
}

void put_lastLapTimes(int secs, int millisecs){
  lastLapTimes[lastLapTimesIndex].millisecs = millisecs;
  lastLapTimes[lastLapTimesIndex].secs = secs;
  lastLapTimesIndex = (lastLapTimesIndex + 1) % TIMES_ARRAY_LIMIT;
}

// use order = 0 for the older lap and TIMES_ARRAY_LIMIT -1 for the last lap.
// TIMES_ARRAY_LIMIT is the same as 0.
int get_secs_lastLapTimes(int order){
  return lastLapTimes[(order + lastLapTimesIndex) % TIMES_ARRAY_LIMIT].secs;
}
// use order = 0 for the older lap and TIMES_ARRAY_LIMIT -1 for the last lap.
// TIMES_ARRAY_LIMIT is the same as 0.
int get_millisecs_lastLapTimes(int order){
  return lastLapTimes[(order + lastLapTimesIndex) % TIMES_ARRAY_LIMIT].millisecs;
}

void update_laps_str(){
  int laps_index = 0;
  for (int i = 0; i < TIMES_ARRAY_LIMIT; i++){
    int line_char_count = snprintf(laps_str + laps_index, 
                                   TEMP_STRING_LENGTH - laps_index,
                                   "%02d.%03d</br>\n",
                                   get_secs_lastLapTimes(i),
                                   get_millisecs_lastLapTimes(i));
    laps_index = laps_index + line_char_count;
  }
}

void IRAM_ATTR isr() {
  int mill = millis();
  int lastLapMillis = mill - button1.lastPressedMillis;
  if (lastLapMillis > ANTIREB_MILLIS) {
    button1.numberKeyPresses += 1;
    button1.pressed = true;
    int lastLapTimeSecs  = lastLapMillis / 1000;
    int lastLapTimeMillis  = lastLapMillis % 1000;
    button1.lastPressedMillis = mill;
    put_lastLapTimes(lastLapTimeSecs, lastLapTimeMillis);
  }
}

WebServer server(80);

void handleRoot() {
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  update_laps_str();

  snprintf(temp, TEMP_STRING_LENGTH,

           "<html>\
  <head>\
    <meta http-equiv='refresh' content='1'/>\
    <title>ESP32 Demo</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1></h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
    <h2>Laps:</h2>\
    <p>%s</p>\
  </body>\
</html>",

           hr, min % 60, sec % 60, laps_str
          );
  server.send(200, "text/html", temp);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp32")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");
  init_lastLapTimes();
  attachInterrupt(button1.PIN, isr, FALLING);
}

void loop(void) {
  server.handleClient();
  if (button1.pressed) {
    Serial.printf("Button 1 has been pressed %u times\n", button1.numberKeyPresses);
    for (int i = 0; i < TIMES_ARRAY_LIMIT; i++){
      Serial.printf("%02d  %03d.%03d\n", i,
                     get_secs_lastLapTimes(i), get_millisecs_lastLapTimes(i));
    }
    button1.pressed = false;
  }
  if (digitalRead(button)){
    digitalWrite(led, 1);
  }
  else {
    digitalWrite(led, 0);
  }
}

